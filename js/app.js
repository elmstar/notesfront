var bk = 'http://ingru.back.ultra/';// Адрес Backend
$(document).ready(function(){
    $.ajax({
        url:bk,
        type: 'get',
        success: function (result) {
            result = JSON.parse(result);
            var resultDataArray = result["data"];
            content_update(resultDataArray);
        },
    });
    // Нажатие на Зелёную кнопку - форма Создания новой записи
    $('#add_art_button').click(function () {
        $('#add_art').css('top', 30+$(window).scrollTop()+'px');
        $('#add_art').toggle();
    });
    // Нажатие на кнопку Сортировка по дате
    $('#app').on('click', '#sort_by_date', function () {
        $.ajax({
            url:bk,
            data: {"sort_date": true},
            type: 'get',
            success: function (result) {
                result = JSON.parse(result);
                var resultDataArray = result["data"];
                content_update(resultDataArray);
            },
        });
    });
    // Сортировка по Избранным
    $('#app').on('click', '#sort_by_favorite', function () {
        $.ajax({
            url:bk,
            data: {"sort_favorite": true},
            type: 'get',
            success: function (result) {
                result = JSON.parse(result);
                var resultDataArray = result["data"];
                content_update(resultDataArray);
            },
        });
    });
    // отработка события Отпрака формы создания новой Записи
    $('#app').on('submit', '#add_art_form', function (e) {
        $.ajax({
            url: bk+'create',
            type: "POST",
            dataType: "html",
            data: $(this).serialize(),
            success: function(response) {
                result = $.parseJSON(response);

            },
            error: function(response) {

            }
        });
        $(this).parent().parent().hide();
        $.ajax({
            url:bk,
            type: 'get',
            success: function (result) {
                result = JSON.parse(result);
                var resultDataArray = result["data"];
                content_update(resultDataArray);
            },
        });
        e.preventDefault();
    });
    // Событие нажатия галочки Избранное
    $('#app').on('click', '.set_favorites', function () {
        var data_send = {'id': $(this).parent().parent().attr('ident')};
        if ($(this).attr('checked') == 'checked') {
            data_send.checked = true;
        } else {
            data_send.checked = false;
        }
        $.ajax({
            url: bk+'favorite',
            data: data_send
        });
    });
    // Нажатие кнопки Редактирования записи
    $('#app').on('click', '.edit', function () {
        $('#edit_art').css('top', 30+$(window).scrollTop()+'px');
        $('#edit_art').toggle();
        $.ajax({
            url:bk+'edit',
            data: {"id": $(this).attr('ident')},
            success: function (result) {
                $('input[name=id]').val(result['id']);
                $('input[name="name"]').val(result['name']);
                $('#edit_description').val(result['description']);
            }
        });
    });
    // Отправка формы редактирования Записи
    $('#app').on('submit', '#edit_art_form', function (e) {
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            dataType: "html",
            data: $(this).serialize(),
            success: function(response) {
                result = $.parseJSON(response);

            },
            error: function(response) {

            }
        });
        $(this).parent().parent().hide();
        $.ajax({
            url:bk,
            type: 'get',
            success: function (result) {
                result = JSON.parse(result);
                var resultDataArray = result["data"];
                content_update(resultDataArray);
            },
        });
        e.preventDefault();
    });
    // Удаление записи
    $('#app').on('click', '.delete', function () {
        $.ajax({
            url:bk+'delete',
            data: {"id": $(this).attr('ident')},
            success: function () {
                $.ajax({
                    url:bk,
                    type: 'get',
                    success: function (result) {
                        result = JSON.parse(result);
                        var resultDataArray = result["data"];
                        content_update(resultDataArray);
                    },
                });
            }
        });
    });

    $('#app').on('click', '.element_name', function () {
        $('#view_art').css('top', 30+$(window).scrollTop()+'px');
        $('#view_art').toggle();
        console.log($(this).next().next().next().next().children().attr('ident'));
        $.ajax({
            url:bk+'edit',
            data: {"id": $(this).next().next().next().next().children().attr('ident')},
            success: function (result) {
                $('#view_name').text(result['name']);
                $('#view_created_at').text(result['created_at']);
                $('#view_description').text(result['description']);
            }
        });
    });

    function ajax_bk(url, data, type = 'get', dataType = null) {
        $.ajax({
            url:url,
            type: type,
            data: data,
            success: function (result) {

            }
        });
    }
    // Построение списка записей
    function content_update(data) {
        $('#content').empty();
        $('#content').append('<div class="header">\n' +
            '                    <div class="header_name cell">Название</div>\n' +
            '                    <div class="header_description cell">Описание</div>\n' +
            '                    <div class="header_created_at cell">Дата создания</div>\n' +
            '                    <div class="header_favorite cell">Избранное</div>\n' +
            '                    <div class="header_action cell">Действия</div>\n' +
            '                </div>');
        data.forEach(function (value, index, array) {
            $('#content').append('<div class="record" ident="'+value['id']+'">' +
                '<div class="element_name cell">'
                + value['name'] +
                '</div>' +
                '<div class="element_description cell">'
                + value['description'] +
                '</div>'+
                '<div class="element_created_at cell">'
                +value['created_at']+
                '</div><div class="element_favorite cell">'+
                '<input type="checkbox" '+(value['favorite'] ? "checked" : "")+' class="set_favorites"></div>' +
                '<div class="element_action cell" ident="'+value['id']+'">' +
                '<div class="action edit" ident="'+value['id']+'"></div>' +
                '<div class="action delete" ident="'+value['id']+'"></div>' +
                '</div>');
        });
    }

    $(document).mouseup(function (e){ // закрытие по клику за пределами элемента
        var closeableTagPlace = $('#view_art');
        if (!closeableTagPlace.is(e.target)
            && closeableTagPlace.has(e.target).length === 0) {
            closeableTagPlace.hide();
        }
        var closeableTagPlace = $('#edit_art');
        if (!closeableTagPlace.is(e.target)
            && closeableTagPlace.has(e.target).length === 0) {
            closeableTagPlace.hide();
        }
        var closeableTagPlace = $('#add_art');
        if (!closeableTagPlace.is(e.target)
            && closeableTagPlace.has(e.target).length === 0) {
            closeableTagPlace.hide();
        }
    });
    //var scrollTarget = $('footer');
    //let counter = 0;

});
